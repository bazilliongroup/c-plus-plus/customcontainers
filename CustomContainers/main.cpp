/*
*
*	Custom Containers
*
*/

#include <stddef.h>
#include <iostream>

#define MAX_SIZE 10

static int intArrayTest[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
static char* charArrayTest[] = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };

template< typename T, int size > class ArrayBuffer
{
public:

	enum{TOP_OF_CONTAINER = 0};

	T* operator[](int index)
	{
		return m_array[index];
	}

	void ClearContainer()
	{
		m_next_empty = 0;

		for (int i = 0; i < size; i++)
		{
			m_array[i] = NULL;
		}
	}

	T* GetElement(int index)
	{
		T* ret = NULL;

		if ((index >= TOP_OF_CONTAINER) && (index < size))
		{
			ret = m_array[index];
		}

		return ret;
	}

	bool RegisterElement(T* theElement)
	{
		bool ret = false;

		if (m_next_empty < size)
		{
			m_array[m_next_empty] = theElement;
			m_next_empty++;
			ret = true;
		}

		return ret;
	}

	ArrayBuffer()
	{
		ClearContainer();
	}

	~ArrayBuffer() {}

private:

	int m_next_empty;
	T* m_array[size];

	ArrayBuffer(const ArrayBuffer&);
	ArrayBuffer operator= (const ArrayBuffer&);
};

int main()
{
	ArrayBuffer<int, MAX_SIZE> intBuffer;
	ArrayBuffer<char, MAX_SIZE> charBuffer;

	for (int i = 0; i < MAX_SIZE; i++)
	{
		intBuffer.RegisterElement(&intArrayTest[i]);
		charBuffer.RegisterElement(charArrayTest[i]);
	}

	for (int i = 0; i < MAX_SIZE; i++)
	{
//		printf("Element is %d\n", *intBuffer.GetElement(i));
		printf("Element is %d\n", *intBuffer[i]);
	}

	printf("\n");

	for (int i = 0; i < MAX_SIZE; i++)
	{
//		printf("Element is %c\n", *charBuffer.GetElement(i));
		printf("Element is %c\n", *charBuffer[i]);
	}

	return 1;
}